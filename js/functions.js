$(function () {
    $(document).scroll(function () {
      var $nav = $(".navbar-fixed-top");
      $nav.toggleClass('scrolled', $(document).scrollTop() > $nav.height());
    });
  });

window.onload = function() {
    scrollTo(0,0);
}